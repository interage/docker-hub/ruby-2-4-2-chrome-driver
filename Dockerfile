FROM ruby:2.4.2

ENV CHROMEDRIVER_DOWNLOAD_URL=https://chromedriver.storage.googleapis.com/73.0.3683.68/chromedriver_linux64.zip

RUN \
  curl -sS -L https://dl.google.com/linux/linux_signing_key.pub | apt-key add - && \
  echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list && \
  apt-get update -qy && \
  apt-get install -y google-chrome-stable nodejs unzip libnss3 && \
  curl $CHROMEDRIVER_DOWNLOAD_URL -o chromedriver.zip && \
  unzip chromedriver.zip -d /bin/
